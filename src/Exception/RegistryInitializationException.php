<?php

namespace TarzanychPatterns\Exception;

/**
 * Used for errors during application initialization.
 */
class RegistryInitializationException extends \RuntimeException { }