<?php

namespace TarzanychPatterns\Registry;

/**
 * Interface for Application Registry implementation.
 */
interface RegistryImplementorInterface {

    /**
     * Getter for registry parameter.
     *
     * @param string $key
     *   Parameter key.
     *
     * @return mixed
     *   Parameter value.
     */
    public function get(string $key);

    /**
     * setter for registry parameter.
     *
     * @param string $key
     *   Parameter key.
     * @param mixed $value
     *   Parameter value.
     *
     * @return void
     */
    public function set(string $key, $value);

    /**
     * Deletes particular parameter from registry.
     *
     * @param string $key
     *   Parameter key.
     *
     * @return void
     */
    public function delete(string $key);

    /**
     * Truncates application registry.
     *
     * @return void
     */
    public function truncate();

}