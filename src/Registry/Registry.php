<?php

namespace TarzanychPatterns\Registry;

use TarzanychPatterns\Exception\RegistryInitializationException;

/**
 * Application registry that uses Bridge pattern for different implementations.
 */
class Registry {

    /**
     * Single instance of Registry.
     *
     * @var \TarzanychPatterns\Registry\Registry
     */
    protected static $instance;

    /**
     * Single instance of Registry.
     *
     * @var \TarzanychPatterns\Registry\RegistryImplementorInterface
     */
    protected $implementor;

    /**
     * Protected Registry constructor.
     */
    protected function __construct() { }

    /**
     * Registry instance getter.
     *
     * @return \TarzanychPatterns\Registry\Registry
     *   Registry instance.
     */
    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    /**
     * Returns TRUE if registry implementor is set.
     *
     * @return bool
     *   Registry initialization state.
     */
    public function isInitialized() {
        return isset($this->implementor);
    }

    /**
     * Registry implementation initialization method.
     *
     * @param string $type
     *   Registry implementor type.
     * @param array $options
     *   Registry implementor options.
     */
    public function initImplementor(string $type, array $options) {
        if ($this->implementor) {
            throw new RegistryInitializationException('Implementor already initialized.');
        }

        // @todo Implement dynamic plugins for registry storage implementations.
        switch ($type) {
            case 'file':
                $this->implementor = new FileRegistryImplementor($options);
                break;

            default:
                throw new RegistryInitializationException('Incorrect registry storage type.');

        }
    }

    /**
     * Registry parameter getter.
     *
     * @param string $key
     *   Parameter key.
     * @param mixed $default_value
     *   Default value.
     *
     * @return mixed
     *   Registry parameter value.
     */
    public function get(string $key, $default_value = NULL) {
        return $this->implementor->get($key) ?? $default_value;
    }

    /**
     * Registry parameter setter.
     *
     * @param string $key
     *   Parameter key.
     * @param mixed $value
     *   Default value.
     *
     * @todo Implement save() method.
     *
     * @return $this
     *   Registry instance.
     */
    public function set(string $key, $value) {
        $this->implementor->set($key, $value);

        return $this;
    }

    /**
     * Registry parameter remover.
     *
     * @param string $key
     *   Parameter key.
     *
     * @todo Implement save() method.
     *
     * @return $this
     *   Registry instance.
     */
    public function delete(string $key) {
        $this->implementor->delete($key);

        return $this;
    }

    /**
     * Registry cleaner.
     *
     * @return $this
     *   Registry instance.
     */
    public function truncate() {
        $this->implementor->truncate();

        return $this;
    }

}