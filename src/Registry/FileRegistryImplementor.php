<?php

namespace TarzanychPatterns\Registry;

use Symfony\Component\Filesystem\Filesystem;
use TarzanychPatterns\Exception\RegistryInitializationException;

/**
 * File storage registry implementor.
 */
class FileRegistryImplementor implements RegistryImplementorInterface {

    /**
     * Symfony Filesystem component.
     *
     * @var \Symfony\Component\Filesystem\Filesystem
     */
    protected $fs;

    /**
     * Registry data filename.
     *
     * @var string
     */
    protected $filename;

    /**
     * FileRegistryImplementor constructor.
     *
     * @param array $options
     *   Registry implementor options.
     *
     * @throws \TarzanychPatterns\Exception\RegistryInitializationException
     *   Thrown if Registry options are incorrect or Registry is already initialized.
     *
     * @todo Move to abstract class.
     */
    public function __construct(array $options) {
        if (Registry::getInstance()->isInitialized()) {
            throw new RegistryInitializationException('Registry is already initialized.');
        }

        // Check registry implementor options.
        if (!self::validateOptions($options)) {
            throw new RegistryInitializationException('Invalid options for File Registry.');
        }

        $this->fs = new Filesystem();
        $this->filename = $options['filename'];

        if (!$this->fs->exists($this->filename)) {
            $dirname = dirname($this->filename);

            try {
                if ($dirname && !$this->fs->exists($dirname)) {
                    $this->fs->mkdir($dirname);
                }

                $this->fs->dumpFile($this->filename, serialize([]));
            }
            catch (\Exception $e) {
                throw new RegistryInitializationException('Could not create registry file.');
            }
        }
    }

    /**
     * Registry storage options validator.
     *
     * @param array $options
     *   Array of options.
     *
     * @return bool
     *   Returns TRUE if options are valid.
     */
    public static function validateOptions(array $options) {
        if (!isset($options['filename']) || !is_string($options['filename'])) {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * Helper method to return registry data from the file
     * 
     * @return array
     *   Registry data.
     */
    protected function getData() {
        if ($this->fs->exists($this->filename)) {
            $data = unserialize(file_get_contents($this->filename));
            
            if (is_array($data)) {
                return $data;
            }
        }

        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $key) {
        return $this->getData()[$key] ?? NULL;
    }

    /**
     * {@inheritdoc}
     */
    public function set(string $key, $value) {
        $data = $this->getData();
        $data[$key] = $value;
        $this->fs->dumpFile($this->filename, serialize($data));
    }

    /**
     * {@inheritdoc}
     */
    public function delete(string $key) {
        $data = $this->getData();
        unset($data[$key]);
        $this->fs->dumpFile($this->filename, serialize($data));
    }

    /**
     * {@inheritdoc}
     */
    public function truncate() {
        $this->fs->dumpFile($this->filename, serialize([]));
    }
}