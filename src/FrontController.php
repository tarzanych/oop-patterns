<?php

namespace TarzanychPatterns;

use Symfony\Component\Yaml\Yaml;
use TarzanychPatterns\Registry\Registry;

/**
 * Front controller of the application.
 *
 * @package TarzanychPatterns
 */
class FrontController {

    /**
     * A single instance of front controller.
     *
     * @var \TarzanychPatterns\FrontController;
     */
    protected static $instance;

    /**
     * Registry instance.
     *
     * @var \TarzanychPatterns\Registry\Registry
     */
    protected $registry;

    /**
     * Protected FrontController constructor, so instance cannot be created by new FrontController operand.
     */
    protected function __construct() { }

    /**
     * Singleton instance getter.
     *
     * @return FrontController
     *   FrontController instance.
     */
    protected static function getInstance() {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Application initialization stack.
     */
    protected function init() {
        if (!file_exists('config.yml')) {
            throw new \RuntimeException('Config file missing.');
        }

        // Parse application configuration.
        $config = Yaml::parse(file_get_contents('config.yml'));

        if (!isset($config['registry']['type']) || !isset($config['registry']['options'])) {
            throw new \RuntimeException('Registry configuration missilg.');
        }

        // Init application registry.
        $this->registry = Registry::getInstance();
        $this->registry->initImplementor($config['registry']['type'], $config['registry']['options']);
    }

    /**
     * Prompt messages and Command execution.
     */
    protected function handleRequest() {
        // Registry test
        $this->registry->set('last_run', time());
    }

    /**
     * Front Controller entrypoint.
     */
    public static function run() {
        $application = self::getInstance();

        $application->init();
        $application->handleRequest();
    }
}