<?php

/**
 * Experimental project created to get more familiar with OOP patterns.
 *
 * @author Serhii Skrypchuk <tarzanych@gmail.com>
 */

use TarzanychPatterns\FrontController;

require_once __DIR__ . '/vendor/autoload.php';

// Run the application.
FrontController::run();
